﻿module world.world;

import derelict.sdl2.sdl;

import Engine;
import world.Space;
import core.InputSystem;
import core.LogicSystem;
import core.GraphicsSystem;
import entity.ObjectFactory;

import std.stdio;
import entity.InputComponent;
import entity.PlayerComponent;
import core.GuiSystem;
import gui.GuiContainer;
import gui.GuiText;
import gui.GuiSelectionContainer;
import gui.GuiFactory;


class World
{
	Engine game;

	InputSystem input;
	LogicSystem logic;
	GraphicsSystem graphics;
	GuiSystem gui;

	ObjectFactory objectFactory;
	GuiFactory guiFactory;


	this(Engine game)
	{
		this.game = game;

	}

	void init()
	{
		game.createSpace("Menu");
		game.createSpace("Main");

		//objectFactory.addPlayer(objectFactory.Player.PLAYER1, 0, 0);
		//objectFactory.addPlayer(objectFactory.Player.PLAYER2, 150, 150);

		objectFactory = new ObjectFactory(this);
		guiFactory = new GuiFactory(game.getRenderer());

		input = new InputSystem(game.input);
		logic = new LogicSystem();
		graphics = new GraphicsSystem(game.getRenderer());
		gui = new GuiSystem();

		Space menuSpace = game.getSpace("Menu");

		guiFactory.createMenuSpace(menuSpace);

		menuSpace.addObject(objectFactory.createPlayer(ObjectFactory.Player.PLAYER1, 0, 0));
		menuSpace.addObject(objectFactory.createPlayer(ObjectFactory.Player.PLAYER2, 150, 150));
		menuSpace.active = true;

		Space mainSpace = game.getSpace("Main");

		writeln("Length of objects with Input: ", menuSpace.getObjects!PlayerComponent.length);
	}

	/*
	 * Update game logic
	 */
	void update(Space space)
	{
		input.update(space);
		logic.update(space);
		gui.update(space);
	}
	
	/*
	 * Render game logic
	 */
	void render(Space space)
	{
		graphics.render(space);
	}

	ObjectFactory getObjectFactory()
	{
		return objectFactory;
	}

	SDL_Renderer * getRenderer()
	{
		return game.getRenderer();
	}
}