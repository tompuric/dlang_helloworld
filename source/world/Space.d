﻿module world.Space;
import entity.GameObject;

import std.string;
import entity.InputComponent;
import io.controller.Device;
import Engine;

import std.stdio;
import std.conv;
import std.algorithm.iteration : filter;
import std.array: array;
import entity.Component;
import gui.GuiContainer;

class Space
{

	private GameObject[] objects;
	private GuiContainer[] gui;
	private string name;
	private Engine game;
	bool active = false;

	enum SpaceType
	{
		Game,
		Menu,
		Overlay
	}

	this(Engine game, string name)
	{
		this.game = game;
		this.name = name;
	}

	string getName()
	{
		return name;
	}

	void addObject(GameObject object)
	{
		objects ~= object;
	}

	void addObject(GameObject[] objects)
	{
		this.objects ~= objects;
	}

	void mergeSpace(Space space)
	{
		addObject(space.getObjects());
	}

	GameObject[] getObjects()
	{
		return objects;
	}

	GameObject[] getObjects(T : Component)()
	{
		GameObject[] gatheredObjects;
		foreach (object; objects)
		{
			Component component = object.get!T();
			if (component)
			{
				gatheredObjects ~= object;
			}
		}
		return gatheredObjects;
	}

	// A device was added to the space
	void addDevice(Device device)
	{
		/*
		 * Gothrough each object and see if an object is requesting for an input. If so, give it to them
		 * 
		 * 
		 * */
		foreach (object; objects)
		{
			writeln("Found object: ", object);

			InputComponent input = object.get!InputComponent;
			if (!input) continue;
			if (input.needsDevice())
			{
			
				input.attach(device);
				break;
			}
		}

		writeln("Device was not connected to anything");
		//assert(false, "Device was not connected to anything");
		// If no object was found. Create one? But which object to create?
		/*
		objects ~= game.getObjectFactory().createPlayerInput(device);
		*/
		 
		foreach(container; gui)
		{
			if (container.selected)
			{
				container.addDevice(device);
			}
		}

	}

	void addGuiContainer(GuiContainer container)
	{
		container.initialise(this);
		gui ~= container;
	}

	GuiContainer[] getActiveGuiContainers()
	{
		return gui.filter!(a => a.selected).array;
	}

	GuiContainer[] getGuiContainers()
	{
		return gui;
	}

	Engine getEngine() {
		return game;
	}
}

