﻿module sdl;

import derelict.sdl2.sdl;
import derelict.sdl2.image;
import derelict.sdl2.mixer;
import derelict.sdl2.ttf;
import derelict.sdl2.net;
import derelict.sdl2.types;

import std.string;
import std.stdio;

class sdl
{

public:
	SDL_Window * window;
	SDL_Renderer * ren;


	this()
	{
		// Constructor code
		// Load Derelict libraries
		DerelictSDL2.load();
		DerelictSDL2Image.load();
		DerelictSDL2Mixer.load();
		DerelictSDL2ttf.load();
		DerelictSDL2Net.load();
	}

	~this()
	{
		SDL_DestroyWindow(window);
		SDL_DestroyRenderer(ren);
	}

	public void init()
	{


		// Initialize SDL2
		if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
		{
			throw new Exception("SDL_Init failed!");
		}
		
		// Initialize TFF
		if (TTF_Init() != 0)
		{
			throw new Exception("TFF_Init failed!");
		}


		
		// Create an application window with the following settings:
		window = SDL_CreateWindow(
			"An SDL2 window".toStringz,                 // window title
			SDL_WINDOWPOS_CENTERED,           			// initial x position
			SDL_WINDOWPOS_CENTERED,          			// initial y position
			640,                               			// width, in pixels
			480,                               			// height, in pixels
			SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL                  // flags - see below
			);
		
		if (window == null){
			SDL_Quit();
			throw new Exception("Window init failed!");
		}
		
		// Create a renderer object
		ren = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (ren == null){
			SDL_DestroyWindow(window);
			SDL_Quit();
			throw new Exception("Renderer init failed!");
		}
		
		SDL_SetRenderDrawColor( ren, 50, 50, 50, 255 );
		SDL_RenderClear( ren );

	}

	public SDL_Renderer* getRenderer()
	{
		return ren;
	}
}

