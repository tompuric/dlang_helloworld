﻿module gui.GuiContainer;
import gui.IContainer;
import world.world;
import derelict.sdl2.types;
import gui.GuiComponent;
import io.controller.Device;
import gui.GuiObject;
import world.Space;
import std.conv;
import std.stdio;

abstract class GuiContainer : GuiObject
{
	enum Axis { HORIZONTAL, VERTICAL };
	enum Direction { LEFT = 0b00, RIGHT, UP, DOWN};

	Axis axis;
	Anchor anchor;
	GuiComponent[] children;
	GuiContainer[4] connections;
	Device[] devices;
	Space space;

	int index;
	bool isActive;


	World world; // reference to world or space

	this(SDL_Renderer * renderer, Axis axis, int x, int y)
	{
		// Constructor code
		super(renderer);
		this.axis = axis;
		this.x = x;
		this.y = y;
		width = 100;
		height = 50;
		selected = false;
		index = 0;
	}

	this(SDL_Renderer * renderer, Axis axis, int x, int y, int width, int height)
	{
		this(renderer, axis, x, y);
		this.width = width;
		this.height = height;
	}

	void initialise(Space space) {
		this.space = space;

		// Enable first element in list
		if (children.length > 0)
			children[0].setSelected(true);
	}

	void addContainer(GuiComponent component)
	{
		component.initialise(this);

		// TODO: Perhaps move this up to GuiFactory?
		if (children.length == 0)
		{
			component.setSelected(true);
		}

		children ~= component;
	}

	void detachContainer(GuiContainer container)
	{
	}

	void addDevice(Device device)
	{
		devices ~= device;
	}

	void interact()
	{
	}

	void update()
	{
		foreach(child; children)
		{
			child.update();
		}
	}

	void render()
	{
		foreach(child; children)
		{
			child.render();
		}
	}

	GuiComponent[] getChildren()
	{
		return children;
	}

	void connect(GuiContainer container, Direction direction)
	{
		assert(connections[direction] is null);
		connections[direction] = container;
		// Switch direction and assign this to container
		container.connections[to!Direction(direction^0b01)] = this;
	}

}

