﻿module gui.GuiSelectionContainer;
import gui.GuiContainer;
import derelict.sdl2.types;
import io.InputController;
import std.stdio;
import std.conv;
import io.controller.Device;
import world.Space;

class GuiSelectionContainer : GuiContainer
{
	private int previousIndex;

	this(SDL_Renderer * renderer, Axis axis, int x, int y)
	{
		super(renderer, axis, x, y);
		previousIndex = 0;
	}

	override void update()
	{
		super.update();

		Device[] devices2 = space.getEngine().input.devices;
		foreach(device; devices2)
		{
			InputController input = device.controller;

			if (input.enter.pressed)
			{
				writeln("Enter pressed on index: ", index);
				children[index].selected();
			}

			if (input.down.pressed)
			{
				move(Direction.DOWN);

			}
			if (input.up.pressed)
			{
				move(Direction.UP);
			}

			if (input.left.pressed)
			{
				move(Direction.LEFT);
			}
			if (input.right.pressed)
			{
				move(Direction.RIGHT);
			}
		}

	}

	override void render()
	{
		super.render();
	}

	/*
	 * direction 	-- direction we signalled to move
	 * */
	private void move(Direction direction)
	{
		int val;

		// If direction is the same as the current axis (maybe seperate into it's own function?
		if (isDirectionAlongAxis(direction)) {
			if (direction is Direction.DOWN || direction is Direction.RIGHT) {
				val = index + 1;
			}
			else {
				val = index - 1;
			}
		}

		// Index goes out of our array bounds -OR- is in the direction opposite to our current axis
		bool outOfBounds = val >= children.length.to!int || val < 0 || !isDirectionAlongAxis(direction);
		if (outOfBounds) {

			writeln("We are out of bounds");
			// We've now reached an out of bounds section of our GUI container

			// The direction we travelled contains a connection
			if (connections[direction] !is null) {
				// Switch to that container
				switchContainer(direction);
			} else {
				// There doesn't exist another container for us to enter. Instead, let's Loop around
				checkBounds(val);
				highlight();
			}
		} else {
			index = val;
			highlight();
		}
	}

	/*
	 * Checks the bounds of int value, if out of bounds, sets index.
	 * Seems dodgy.
	 * TODO: Seperate to just be a boolean and set index elsewhere
	 * */
	private void checkBounds(int value)
	{
		// Adjust index values
		if (value >= children.length.to!int)
		{
			index = 0;
		}
		if (value < 0)
		{
			index = (children.length - 1).to!int;
		}
	}

	/*
	 * Deselect the old container
	 * Select the new container
	 * TODO: Make it autonomous to its name. Highlight shouldn't be unlighting objects
	 * */
	private void highlight() {
		children[previousIndex].setSelected(false);
		children[index].setSelected(true);
		previousIndex = index;
	}

	/* 
	 * Switch the Gui Container.
	 * 	- Direction: Switches the container in the direction given
	 *  - Value: value used to determine which value the new contianer should maintain. 
	 * */
	protected void switchContainer(Direction direction)
	{	
		GuiContainer container = connections[direction];
		if (container !is null)
		{
			this.setSelected(false);
			container.setSelected(true);

			/* 
			 * The following code sets the index value to be appropriate to what's needed.
			 * TODO: Fix this up. Make it Axis friendly
			 * */
			if (direction is direction.DOWN) {
				container.index = 0;
			}
			if (direction is direction.UP) {
				container.index = container.children.length.to!int - 1;
			}

			if(direction is direction.LEFT || direction is direction.RIGHT)
			{
				if (!isDirectionAlongAxis(direction)) {
					container.index = index;
				}
			}

			container.to!GuiSelectionContainer.highlight();
		}
		
	}

	/*
	 * Checks if the direction is along the containers current axis
	 * Can improve toone line if using binary manipulation
	 * */
	private bool isDirectionAlongAxis(Direction direction)
	{
		Axis tempAxis;

		if (direction is Direction.LEFT || direction is Direction.RIGHT)
			tempAxis = Axis.HORIZONTAL;
		if (direction is Direction.UP || direction is Direction.DOWN)
			tempAxis = Axis.VERTICAL;

		return axis is tempAxis;
	}

}

