﻿module gui.GuiObject;
import derelict.sdl2.types;

class GuiObject
{
	SDL_Renderer * renderer;
	SDL_Rect dst;
	int x;
	int y;
	int width;
	int height;

	bool selected;

	this(SDL_Renderer * renderer)
	{
		this.renderer = renderer;
		selected = false;
	}

	void setSelected(bool select)
	{
		selected = select;
	}

	bool isSelected()
	{
		return selected;
	}
}

