﻿module gui.IContainer;
import derelict.sdl2.types;

enum Anchor
{
	TOP_LEFT,
	TOP_MIDDLE,
	TOP_RIGHT,
	RIGHT,
	LEFT,
	BOTTOM_LEFT,
	BOTTOM_MIDDLE,
	BOTTOM_RIGHT,
	CENTER,
	CENTER_LEFT,
	CENTER_UP,
	CENTER_RIGHT,
	CENTER_DOWN
}

interface IGuiContainer
{
//	void addContainer(IGuiContainer container);
//	void detachContainer(IGuiContainer container);
	void interact();
	void update();
	void render(SDL_Renderer * renderer);
}


