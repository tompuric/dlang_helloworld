﻿module gui.GuiFactory;
import derelict.sdl2.types;
import gui.GuiContainer;
import gui.GuiText;
import world.Space;
import gui.GuiSelectionContainer;

class GuiFactory
{
	SDL_Renderer* renderer;
	
	this(SDL_Renderer* renderer)
	{
		this.renderer = renderer;
	}
	
	T create(T, ARGS...)(ARGS args)
	{
		return new T(renderer, args);
	}


	void createMenuSpace(Space space)
	{
		GuiContainer mainMenu = createMainMenu(space);
		GuiContainer optionsMenu = createOptionsMenu(space);
		GuiContainer optionsMenu2 = createOptions2Menu(space);
		GuiContainer optionsMenu3 = createOptions3Menu(space);
		GuiContainer horizontalMenu = createHorizontalMenu(space);

		mainMenu.connect(optionsMenu, optionsMenu.Direction.RIGHT);
		optionsMenu.connect(optionsMenu2, optionsMenu.Direction.RIGHT);
		optionsMenu.connect(optionsMenu3, optionsMenu.Direction.DOWN);

		//optionsMenu.connect(optionsMenu3, optionsMenu.Direction.UP);
		mainMenu.connect(optionsMenu2, optionsMenu.Direction.LEFT);

		optionsMenu3.connect(horizontalMenu, optionsMenu.Direction.DOWN);
		horizontalMenu.connect(optionsMenu, optionsMenu.Direction.DOWN);
	}

	private GuiContainer createMainMenu(Space space)
	{
		GuiContainer container = create!GuiSelectionContainer(GuiContainer.Axis.VERTICAL, 80, 70);
		container.addContainer(create!GuiText(10, 10, "Hello"));
		container.addContainer(create!GuiText(10, 40, "There"));
		container.addContainer(create!GuiText(10, 70, "Newb"));
		container.setSelected(true);
		space.addGuiContainer(container);
		return container;
	}

	private GuiContainer createOptionsMenu(Space space)
	{
		GuiContainer container = create!GuiSelectionContainer(GuiContainer.Axis.VERTICAL, 200, 70);
		container.addContainer(create!GuiText(10, 10, "Hi"));
		container.addContainer(create!GuiText(10, 40, "Here"));
		container.addContainer(create!GuiText(10, 70, "Pro"));
		space.addGuiContainer(container);
		return container;
	}

	private GuiContainer createOptions2Menu(Space space)
	{
		GuiContainer container = create!GuiSelectionContainer(GuiContainer.Axis.VERTICAL, 300, 70);
		container.addContainer(create!GuiText(10, 10, "Hi2"));
		container.addContainer(create!GuiText(10, 40, "Here2"));
		container.addContainer(create!GuiText(10, 70, "Pro2"));
		space.addGuiContainer(container);
		return container;
	}

	private GuiContainer createOptions3Menu(Space space)
	{
		GuiContainer container = create!GuiSelectionContainer(GuiContainer.Axis.VERTICAL, 200, 270);
		container.addContainer(create!GuiText(10, 10, "Hi3"));
		container.addContainer(create!GuiText(10, 40, "Here3"));
		container.addContainer(create!GuiText(10, 70, "Pro3"));
		space.addGuiContainer(container);
		return container;
	}

	private GuiContainer createHorizontalMenu(Space space)
	{
		GuiContainer container = create!GuiSelectionContainer(GuiContainer.Axis.HORIZONTAL, 200, 400);
		container.addContainer(create!GuiText(10, 10, "Hi4"));
		container.addContainer(create!GuiText(70, 10, "Here4"));
		container.addContainer(create!GuiText(130, 10, "Pro4"));
		space.addGuiContainer(container);
		return container;
	}
}

