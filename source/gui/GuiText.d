﻿module gui.GuiText;

import std.stdio;
import std.string;
import derelict.sdl2.types;
import gui.GuiComponent;
import derelict.sdl2.ttf;
import derelict.sdl2.functions;
import derelict.sdl2.image;

import graphics.Font;

class GuiText : GuiComponent
{

	SDL_Color c = { 255, 255, 255 };
	SDL_Color selected_c = {0, 255, 0};
	TTF_Font *f;
	SDL_Surface *selected_bmp;

	SDL_Texture *tex1;
	SDL_Texture *tex2;

	SDL_Surface *bm_tga_sur;
	SDL_Texture *bm_tga_tex;

	Font f;

	//string text;

	this(SDL_Renderer * renderer, int x, int y, string text)
	{
		super(renderer, x, y);
		// Calculate width of text
		//int width = 50;
		this.text = text;
		f = TTF_OpenFont("/res/Arial.ttf", 18);
		bmp = TTF_RenderText_Solid(f, toStringz(text), c);
		selected_bmp = TTF_RenderText_Solid(f, toStringz(text), selected_c);

		tex1 = SDL_CreateTextureFromSurface(renderer, selected_bmp);
		tex2 = SDL_CreateTextureFromSurface(renderer, bmp);

		//SDL_FreeSurface(bmp);
		//SDL_FreeSurface(selected_bmp);

		bm_tga_sur = IMG_Load("res/Arial.tga");
		bm_tga_tex = SDL_CreateTextureFromSurface(renderer, bm_tga_sur);

		SDL_FreeSurface(bm_tga_sur);
		f = new Font();
	}

	~this()
	{
		SDL_FreeSurface(selected_bmp);
	}

	override void update() {

	}

	override void render() {
		if (isSelected() && parent.selected)
		{
			tex = tex1;
		} else
		{
			tex = tex2;
		}


		//SDL_FreeSurface(tex);

		SDL_RenderCopy(renderer, tex, null, &dst);
		SDL_RenderCopy(renderer, bm_tga_tex, null, &dst);
	}

	override void selected()
	{
		writeln("I was interacted with: ", text);
	}
}

