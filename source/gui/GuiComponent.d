﻿module gui.GuiComponent;
import derelict.sdl2.types;
import derelict.sdl2.functions;
import gui.GuiContainer;
import gui.GuiObject;

abstract class GuiComponent : GuiObject
{
	GuiContainer parent;

	string text;

	SDL_Surface *bmp;
	SDL_Texture *tex;

	this(SDL_Renderer * renderer, int x, int y)
	{
		super(renderer);
		this.x = x;
		this.y = y;
		width = 50;
		height = 30;
	}

	~this()
	{
		SDL_FreeSurface(bmp);
	}

	void initialise(GuiContainer parent)
	{
		this.parent = parent;
		x = parent.x + x;
		y = parent.y + y;

		dst.x = x;
		dst.y = y;
		dst.w = width;
		dst.h = height;
	}

	abstract void update()
	{
	}

	abstract void render()
	{
	}

	abstract void selected()
	{
	}
}

