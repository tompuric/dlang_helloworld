﻿module core.LogicSystem;

import entity.LogicComponent;
import entity.GameObject;
import entity.MovementComponent;
import world.Space;
import entity.InputComponent;

class LogicSystem
{
	this()
	{
		// Constructor code
	}

	void update(Space space)
	{
		GameObject[] objects = space.getObjects();
		
		foreach(object; objects)
		{
			if(object.has!LogicComponent)
			{
				object.get!LogicComponent.update();
			}

			if(object.has!MovementComponent)
			{
				object.get!MovementComponent.update();
			}

			if(object.has!InputComponent)
			{
				object.get!InputComponent.update();
			}
		}
	}
}

