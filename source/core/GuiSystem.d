﻿module core.GuiSystem;
import entity.GameObject;
import world.Space;
import entity.TextComponent;
import gui.GuiContainer;
import std.stdio;

class GuiSystem
{
	this()
	{
		// Constructor code
	}

	void update(Space space)
	{
		GuiContainer[] guiContainers = space.getActiveGuiContainers();

		foreach(container; guiContainers)
		{
			container.update();
		}
	}

}

