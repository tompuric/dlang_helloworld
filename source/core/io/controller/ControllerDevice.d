﻿module io.controller.ControllerDevice;

import io.controller.Device;
import Engine;
import derelict.sdl2.types;
import std.stdio;
import std.conv;
import std.algorithm;

import io.InputController;
import derelict.sdl2.functions;
import entity.InputComponent;
import core.io.InputGatherer;

class ControllerDevice : Device
{

public:
	SDL_GameController* device;

	this(InputGatherer input, int deviceID)
	{
		super(input, DeviceType.CONTROLLER);

		// Initialise Controller
		device = SDL_GameControllerOpen(deviceID);
		SDL_Joystick* joystick = SDL_GameControllerGetJoystick(device);
		// SDL_Joystic
		SDL_JoystickID id = SDL_JoystickInstanceID(joystick);
		this.id = id;
		this.player = deviceID;
	}

	public override void register(InputComponent component)
	{
		super.register(component);
		// Attach to some Component
		controller = new InputController(InputType.CONTROLLER);
		controller.attachComponent(component);
	}

	public override void unregister()
	{
		super.unregister();
	}

	~this() // destructor for Foo
	{
		SDL_GameControllerClose(device);
	}

}

