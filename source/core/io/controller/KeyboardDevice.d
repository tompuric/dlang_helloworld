﻿module io.controller.KeyboardDevice;

import io.controller.Device;
import io.InputController;
import std.conv;
import std.stdio;
import entity.InputComponent;
import core.io.InputGatherer;

class KeyboardDevice : Device
{

	this(InputGatherer input)
	{
		super(input, DeviceType.KEYBOARD);
	}

	public override void register(InputComponent component)
	{
		super.register(component);
		controller = new InputController(InputType.KEYBOARD);
	}
	
	public override void unregister()
	{
		super.unregister();
	}
}

