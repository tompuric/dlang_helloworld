﻿module io.controller.Controller;

interface Controller
{

public:
	public abstract void register();
	public abstract void unregister();

}

