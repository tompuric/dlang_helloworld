﻿module io.controller.Device;
import io.InputController;
import entity.InputComponent;

import std.stdio;
import core.io.InputGatherer;


enum DeviceType
{
	KEYBOARD,
	CONTROLLER
}

abstract class Device
{

	public InputController controller;
	public InputGatherer input;
	public const DeviceType type;
	public InputComponent component;

	int player;
	int id;

	public abstract void register(InputComponent component)
	{
		this.component = component;
	}

	public abstract void unregister()
	{
		if (component !is null)
		{
			writeln("Removing component from device");
			component.unregister();
		}
	}

	this(InputGatherer input, DeviceType type)
	{
		this.input = input;
		this.type = type;
		player = 0;
		id = 0;
	}

	void attachComponent(InputComponent component)
	{
		this.component = component;
	}

	int getPlayer()
	{
		return player;
	}

	int getID()
	{
		return id;
	}

	/*
	protected int getNextAvailablePlayerIndex()
	{
		Device[] devices = input.devices;
		for(int i = 0; i < devices.length; i++)
		{
			if (devices[i] is null)
			{
				return i;
			}
		}
		return -1;
	}
	*/

}

