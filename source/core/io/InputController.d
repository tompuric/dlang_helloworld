﻿module io.InputController;

import core.io.InputGatherer;
import entity.InputComponent;

import std.stdio;

class InputController
{

public:
	Action[] actions;
	Action left;
	Action up;
	Action right;
	Action down;
	Action enter;

	// These aren't needed here, might have to factor back to Device
	InputType type;
	private InputComponent component;

	
	this(InputType type)
	{
		this.type = type;

		left = new Action();
		up = new Action();
		right = new Action();
		down = new Action();
		enter = new Action();
	}

	void attachComponent(InputComponent component)
	{
		this.component = component;
	}

	void update()
	{
		foreach(action; actions)
		{
			action.update();
		}
	}
	
	class Action
	{
	public:
		int iterations;
		bool pressed;
		float power;
		bool held;
		
		this()
		{
			actions ~= this;
			pressed = false;
			held = false;
		}

		void update()
		{
			if (held)
			{
				pressed = iterations <= 1;
				iterations++;
			}
		}
		
		void toggle(bool actionPressed, float power)
		{
			// TODO: Differentiate between pressed and held
			if (actionPressed)
			{
				this.power = power;
				held = true;
				iterations++;
			}
			else
			{
				iterations = 0;
				power = 0;
				pressed = false;
				held = false;
			}
		}
	}

}

