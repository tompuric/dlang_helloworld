﻿module core.io.InputGatherer;

import std.conv;
import std.stdio;
import std.algorithm;

import derelict.sdl2.sdl;
import io.InputController;
import io.controller.ControllerDevice;
import Engine;
import io.controller.Device;
import io.controller.KeyboardDevice;
import entity.GameObject;
import entity.InputComponent;
import world.Space;


// Source: https://wiki.libsdl.org/SDL_EventType

enum MODE {
	SINGLE,
	MULTIPLAYER
};

enum InputType {
	CONTROLLER,
	KEYBOARD,
	AI
}


/*
 * Plan of attack:
 * 1 Input system, loops through all the events in one time loop and adjusts the current inputControllers,
 * then calls all the input Controllers with the updated logic.
 * InputComponent then reads the InputController and calls the appropriate methods
 * 
 * We need: 
 * 	- A linking between InputControllers and InputComponent. There is a 1-1 linking here
 *  - A linking between InputControllers and Devices. There is a 1-0..1 linking here
 *  - InputComponent (when added) to register InputControllers (maybe devices)
 *  - InputComponent (when removed) to unregister InputControllers and their associated devices
 * 
 * 
 * 
 * */

class InputGatherer
{
	SDL_Event event;
	Engine game;
	InputController[] controllers;
	//const int MAX_NUM_OF_DEVICES = 2;
	Device[] devices;

	const int JOYSTICK_DEADZONE = 8000;
	const float JOYSTICK_AXISMAX = 32767.0f;
	bool KEYBOARD_ACTIVE = false;
	
	this(Engine game)
	{
		this.game = game;
	}
	
	void update()
	{
		//GameObject[] objects = space.getObjects();
		handleEvents(event);

		foreach(controller; controllers)
		{
			controller.update();
		}

		foreach(device; devices)
		{
			device.controller.update();
		}
		
	}
	
	private void handleEvents(SDL_Event event)
	{
		while(SDL_PollEvent(&event))
		{
			handle(event);
			
			if (event.type == SDL_QUIT)
			{
				game.running = false;
			}		
		}
	}
	
	private void handle(SDL_Event event)
	{
		switch(event.type)
		{
			//			case SDL_MOUSEMOTION:
			//				writefln("Mouse X: %d, Y: %d", event.motion.x, event.motion.y);
			//				break;
			//			case SDL_WINDOWEVENT:
			//				writefln("Window event: %d", event.window.type);
			//				break;
			//			case SDL_KEYDOWN, SDL_KEYUP:
			//				handleKeyboard(event.key);
			//				break;
			// Window events
			case SDL_WINDOWEVENT,
				SDL_SYSWMEVENT:
					// TODO: Window events:
					break;
			// Keyboard
			case SDL_KEYDOWN,
				SDL_KEYUP,
				SDL_TEXTEDITING,
				SDL_TEXTINPUT,
				SDL_KEYMAPCHANGED:
					handleKeyboard(event.key);
			break;
			// Mouse
			case SDL_MOUSEMOTION,
				SDL_MOUSEBUTTONDOWN,
				SDL_MOUSEBUTTONUP,
				SDL_MOUSEWHEEL:
					// TODO: Mouse Input
					break;
			// Joystick
			case SDL_JOYAXISMOTION,
				SDL_JOYBALLMOTION,
				SDL_JOYHATMOTION,
				SDL_JOYBUTTONDOWN,
				SDL_JOYBUTTONUP,
				SDL_JOYDEVICEADDED,
				SDL_JOYDEVICEREMOVED:
					//handleController(event);
					break;
			// Controller
			case SDL_CONTROLLERAXISMOTION,
				SDL_CONTROLLERBUTTONDOWN,
				SDL_CONTROLLERBUTTONUP,
				SDL_CONTROLLERDEVICEADDED,
				SDL_CONTROLLERDEVICEREMOVED,
				SDL_CONTROLLERDEVICEREMAPPED:
					// Don't handle these
					// TODO: Maybe send a message saying controller not supported?
					//writeln("Controller (not joy) input received: ", event);
					handleController2(event);
			break;
			// Touch Events
			case SDL_FINGERDOWN,
				SDL_FINGERUP,
				SDL_FINGERMOTION:
					// TODO: Touch events
					break;
			
			// Audio Events
			case SDL_AUDIODEVICEADDED,
				SDL_AUDIODEVICEREMOVED:
					// TODO: Audio events (headphones added?)
					break;
			default:
				writefln("Unknown event %d", event.type);
				break;
		}
	}
	
	
	/*
	 * PUBLIC FUNCTIONS
	 * */
	
	public void registerController(InputType type, int deviceID = 0)
	{
		Device device;
		
		switch(type) with (InputType) {
			case CONTROLLER:
				device = new ControllerDevice(this, deviceID);
				break;
			case KEYBOARD:
				device = new KeyboardDevice(this);
				break;
			default:
				assert(false, "There should be no other InputType");
		}
		
		devices ~= device;
		game.attachDevice(device);
	}
	
	public void unregisterController(int deviceID)
	{
		// TODO: Fix this
		// TODO: Have a state that maybe switches between the way controllers are plugged in?
		/* May not need to unregister all controllers any more and reattach them? */

		foreach(device; devices)
		{
			//device.unregister();
			if (device.getID() == deviceID)
			{
				device.unregister();
				devices = devices.remove(devices.countUntil(device));
				device = null;
			}
		}
		
		// Lower all.
//		int i = 0;
//		foreach(device; devices)
//		{
//			device.player = i;
//			game.attachDevice(device);
//			i++;
//		}
	}
	
	public Device getDevice(int deviceID, DeviceType type)
	{
		foreach (device; devices)
		{
			if (device.getID() == deviceID && device.type == type)
			{
				return device;
			}
		}
		return null;
	}
	
	/*
	 * PRIVATE FUNCTIONS
	 * */
	
	private void handleController2(SDL_Event event)
	{
		// Get Device Here.
		
		Device device = getDevice(event.cdevice.which, DeviceType.CONTROLLER);
		
		switch(event.type)
		{
			case SDL_CONTROLLERAXISMOTION:
				//writeln("Controller (not joy) axis motion: ", event.cdevice.which);
				
				if (device is null) {
					return;
				}
				if (device.controller is null) {
					return;
				}
				
				InputController controller = device.controller;
				
				float value2 = event.jaxis.value/JOYSTICK_AXISMAX;
				int axis = event.jaxis.axis;
				int value = event.jaxis.value;
				
				
				if (value > -JOYSTICK_DEADZONE && value < JOYSTICK_DEADZONE)
				{
					if (axis == 0)
					{
						controller.left.toggle(false, 0.0f);
						controller.right.toggle(false, 0.0f);
					}
					else if (axis == 1)
					{
						controller.up.toggle(false, 0.0f);
						controller.down.toggle(false, 0.0f);
					}
					break;
				}
				
				// Left Analogue, horizontal axis
				if (axis == 0)
				{
					//writefln("%d : %d", event.jaxis.value, event.jaxis.axis);
					if (value < 0)
					{
						controller.left.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
					else
					{
						controller.right.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
				}
				// Left Analogue, vertical axis
				else if (event.jaxis.axis == 1)
				{
					//writefln("%d : %d", event.jaxis.value, event.jaxis.axis);
					if (value < 0)
					{
						controller.up.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
					else
					{
						controller.down.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
				}
				// Right Analogue, horizontal axis
				else if (event.jaxis.axis == 2)
				{
					//writefln("%d : %d", event.jaxis.value, event.jaxis.axis);
					if (value < 0)
					{
						//left.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
					else
					{
						//left.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
				}
				// Right Analogue, vertical axis
				else
				{
					//writefln("%d : %d", event.jaxis.value, event.jaxis.axis);
					if (value < 0)
					{
						//left.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
					else
					{
						//left.toggle(true, event.jaxis.value/JOYSTICK_AXISMAX);
					}
				}
				break;
			case SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLERBUTTONUP:
				handleController(device, event.cbutton);
				break;
			case SDL_CONTROLLERDEVICEADDED:
				writeln("Controller device added: ", event.cdevice);
				registerController(InputType.CONTROLLER, event.cdevice.which);
				break;
			case SDL_CONTROLLERDEVICEREMOVED:
				unregisterController(event.cdevice.which);
				writeln("Controller (not joy) device removed: ", event.cdevice);
				break;
			case SDL_CONTROLLERDEVICEREMAPPED:
				break;
			default:
				assert(false, "Other SDL_CONTROLLER encountered");
		}
	}
	
	private void handleController(Device device, SDL_ControllerButtonEvent button)
	{
		if (device is null) {
			return;
		}
		
		switch(button.type)
		{
			case SDL_CONTROLLERBUTTONDOWN:
				toggleController(true, button.button, 1);
				break;
			case SDL_CONTROLLERBUTTONUP:
				toggleController(false, button.button, 1);
				break;
			default:
				break;
		}
	}
	
	private void toggleController(bool pressed, int button, int player)
	{
		switch (button)
		{
			case SDL_CONTROLLER_BUTTON_A: // Down
				writeln("CTRL_A");
				break;
			case SDL_CONTROLLER_BUTTON_B: // Right
				writeln("CTRL_B");
				break;
			case SDL_CONTROLLER_BUTTON_Y: // Up
				writeln("CTRL_Y");
				break;
			case SDL_CONTROLLER_BUTTON_X: // Left
				writeln("CTRL_X");
				break;
			case SDL_CONTROLLER_BUTTON_LEFTSHOULDER: // L1
				writeln("CTRL_L1");
				break;
			case SDL_CONTROLLER_BUTTON_LEFTSTICK: // L3
				writeln("CTRL_L3");
				break;		
			case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER: // R1
				writeln("CTRL_R1");
				break;
			case SDL_CONTROLLER_BUTTON_RIGHTSTICK: // R3
				writeln("CTRL_R3");
				break;
			case SDL_CONTROLLER_BUTTON_START: // Start
				writeln("CTRL_START");
				break;
			case SDL_CONTROLLER_BUTTON_BACK: // Select
				writeln("CTRL_SELECT");
				break;
				
			case SDL_CONTROLLER_BUTTON_DPAD_DOWN: // DPAD_Left
				writeln("CTRL_DPAD_DOWN");
				break;
			case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: // DPAD_Right
				writeln("CTRL_DPAD_RIGHT");
				break;
			case SDL_CONTROLLER_BUTTON_DPAD_UP: // DPAD_Up
				writeln("CTRL_DPAD_UP");
				break;
			case SDL_CONTROLLER_BUTTON_DPAD_LEFT: // DPAD_Left
				writeln("CTRL_DPAD_LEFT");
				break;
				
			case SDL_CONTROLLER_BUTTON_GUIDE: // XBOX-BUTTON
				writeln("CTRL_GUIDE");
				break;
			case SDL_CONTROLLER_BUTTON_INVALID: // UNKNOWN?
				writeln("CTRL_INVALID");
				break;
			default:
				assert(false, "Default SDL_CONTROLLER button encountered");
		}
	}
	
	private void handleKeyboard(SDL_KeyboardEvent key)
	{
		if (!KEYBOARD_ACTIVE) KEYBOARD_ACTIVE = true;
		
		switch(key.type)
		{
			case SDL_KEYDOWN:
				toggleKeyboard(true, key.keysym.sym, 1);
				break;
			case SDL_KEYUP:
				toggleKeyboard(false, key.keysym.sym, 1);
				break;
			default:
				break;
		}
	}
	
	private void toggleKeyboard(bool pressed, int sym, int player)
	{
		
		Device device = getDevice(0, DeviceType.KEYBOARD);
		
		if (device is null)
		{
			registerController(InputType.KEYBOARD);
			toggleKeyboard(pressed, sym, player);
			return;
		}
		
		InputController controller = device.controller;
		if (!controller) return;
		
		
		switch(sym)
		{
			case SDLK_LEFT, SDLK_a:
				controller.left.toggle(pressed, 1.0f);
				break;
			case SDLK_UP, SDLK_w:
				controller.up.toggle(pressed, 1.0f);
				break;
			case SDLK_RIGHT, SDLK_d:
				controller.right.toggle(pressed, 1.0f);
				break;
			case SDLK_DOWN, SDLK_s:
				controller.down.toggle(pressed, 1.0f);
				break;
			case SDLK_RETURN: // enter
				controller.enter.toggle(pressed, 1.0f);

				break;
			default:
				break;
		}
		
	}
	
	private void reset()
	{
		
	}
	
	
}


