﻿module core.GraphicsSystem;

import entity.GraphicsComponent;
import entity.GameObject;
import derelict.sdl2.sdl;
import world.Space;
import entity.TextComponent;
import gui.GuiContainer;

class GraphicsSystem
{

public:
	SDL_Renderer * renderer;

	this(SDL_Renderer * renderer)
	{
		this.renderer = renderer;
	}


	void render(Space space)
	{
		GameObject[] objects = space.getObjects();

		foreach(object; objects)
		{

			if (object.has!GraphicsComponent)
			{
				object.get!GraphicsComponent.render(renderer);
			}

			/*
			if (object.has!TextComponent)
			{
				object.get!TextComponent.render(renderer);
			}*/

		}

		GuiContainer[] guiContainers = space.getGuiContainers();

		foreach(container; guiContainers)
		{
			container.render();
		}

	}
}

