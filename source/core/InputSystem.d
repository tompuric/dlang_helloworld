﻿module core.InputSystem;

import entity.LogicComponent;
import entity.GameObject;
import entity.MovementComponent;
import world.Space;
import entity.InputComponent;
import io.controller.Device;
import core.io.InputGatherer;

class InputSystem
{

	InputGatherer input;

	this(InputGatherer input)
	{
		this.input = input;
	}
	
	void update(Space space)
	{
		GameObject[] objects = space.getObjects();
		
		foreach(object; objects)
		{
			if(object.has!InputComponent)
			{
				object.get!InputComponent.update();
			}
		}
	}

	Device[] getDevices()
	{
		return input.devices;
	}
}

