﻿//module entity.GameObject_TEST;

/*
class GameObject
{
	Component[] components;

	this (Component[] components) 
	{
		this.components = components;

		foreach(component; this.components) {
			component.parent = this;
		}
	}

	T get(T : Component)()
	{
		T t;

		foreach(component; components)
		{
			t = cast(T) component;

			if(t !is null)
			{
				return t;
			}
		}

		return null;
	}

	bool has(T : Component)()
	{
		return get!T !is null;
	}

	void attach(Component component)
	{
		this.components ~= component;
	}

	void remove(Component component)
	{
		components.remove(components.countUntil(component));
	}
}


class Component
{

	GameObject parent;

	this()
	{
		// Constructor code
	}
}

class GraphicsComponent : Component
{
	void render();
}

class PlayerGraphicsComponent : GraphicsComponent
{
	override void render() 
	{
		PlayerComponent player = parent.get!PlayerComponent;
		writeln(player.name);
		super.render;
	}
}

class PlayerComponent : Component
{
	string name;

	this(string name)
	{
		this.name = name;
	}

	void update()
	{

	}
}

void main()
{
	GameObject object = new GameObject([ new GraphicsComponent() ]);

	if(object.has!GraphicsComponent)
	{
		object.get!GraphicsComponent.render();
	}

	while(true) {
		update();
		render();
		wait();
		//

		LogicSystem.update(objects);
		GraphicsSystem.render(objects);

	}
}

GameObject createPlayer(string name)
{
	return new GameObject([
			new PlayerComponent(name),
			new PlayerGraphicsComponent()
		]);
}

class GraphicsSystem
{
	void render(GameObject[] objects)
	{
		foreach(object; objects)
		{
			if(object.has!GraphicsComponent)
			{
				object.get!GraphicsComponent.render();
			}
		}
	}
}

class LogicSystem
{
	void render(GameObject[] objects)
	{
		foreach(object; objects)
		{
			if(object.has!GraphicsComponent)
			{
				object.get!GraphicsComponent.render();
			}
		}
	}
}

interface IUpdatable
{
	void update();
}

*/