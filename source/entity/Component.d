﻿module entity.Component;

import entity.GameObject;
import std.string;

class Component
{
	
	GameObject parent;
	
	this()
	{
	}

	void initialize()
	{

	}

	void remove()
	{
		parent.remove(this);
	}

}