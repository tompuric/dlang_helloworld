﻿module entity.InputPlayerComponent;
import entity.InputComponent;
import entity.MovementComponent;

class InputPlayerComponent : InputComponent
{
	this()
	{
		// Constructor code
	}

	override void update()
	{
		if (!input) return;
		
		auto logic = parent.get!MovementComponent;
		//assert(logic !is null, "Missing MovementComponent");
		
		if (logic)
		{
			if (input.left.held)
			{
				logic.moveLeft(input.left.power);
			}
			if (input.right.held)
			{
				logic.moveRight(input.right.power);
			}
			if (input.up.held)
			{
				logic.moveUp(input.up.power);
			}
			if (input.down.held)
			{
				logic.moveDown(input.down.power);
			}
		}
	}
}

