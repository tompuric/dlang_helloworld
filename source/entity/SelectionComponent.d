﻿module entity.SelectionComponent;
import entity.Component;

class SelectionComponent : Component
{
	// Need an Object to store what is currently selected or position in list
	// Maybe: list of items we are going through

	int items;
	int position;

	this(int items)
	{
		position = 0;
	}

	override void initialize()
	{
	}

	void update()
	{
		if (position >= items)
		{
			position = 0;
		}
		if (position < 0)
		{
			position = items - 1;
		}
	}

	void left()
	{
		// go left (if you can)
	}

	void right()
	{
		// go right (if you can)
	}

	void up()
	{
		// Move cursor up
		position--;
	}

	void down()
	{
		// Move cursor down
		position++;
	}
}

