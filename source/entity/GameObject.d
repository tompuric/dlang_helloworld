﻿module entity.GameObject;

import entity.Component;
import std.stdio;
import std.algorithm : countUntil, remove;
import std.format;
import world.world;


class GameObject
{

	int x;
	int y;

	World world;
	Component[] components;

	this (Component[] components)
	{
		this.components = components;

		foreach(component; this.components)
		{
			component.parent = this;
		}
	}

	T get(T : Component)()
	{
		T t;
		
		foreach(component; components)
		{
			t = cast(T) component;
			
			if(t !is null)
			{
				return t;
			}
		}
		
		return null;
	}
	
	bool has(T : Component)()
	{
		return get!T !is null;
	}

	void add(Component component)
	{
		component.parent = this;
		component.initialize();
		components ~= component;
	}

	void remove(Component component)
	{
		components = components.remove(components.countUntil(component));
	}

	void init(World world) {
		this.world = world;

		foreach(component; components)
		{
			component.initialize();
		}
	}

	override string toString()
	{
		return format("%s", components);
	}

}