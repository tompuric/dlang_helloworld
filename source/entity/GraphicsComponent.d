﻿module entity.GraphicsComponent;

import entity.Component;
import derelict.sdl2.sdl;
import std.stdio;
import entity.LogicComponent;
import entity.InputComponent;
import derelict.sdl2.ttf;
import entity.PlayerComponent;
import std.conv;
import std.string;

import world.world;

class GraphicsComponent : Component
{

	SDL_Surface *bmp;
	SDL_Texture *tex;

	SDL_Color c = { 255, 255, 255 };
	TTF_Font *f;

	this()
	{
	}

	~this()
	{
		SDL_FreeSurface(bmp);
	}

	override void initialize()
	{
		/*
		 * To fix the bmp/txt SDL_CreateTextureFromSurface, we need to make a
		 *"SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren)"
		 * file */
		 
		//bmp = SDL_LoadBMP("res/background.bmp");

		f = TTF_OpenFont( "/Library/Fonts/Arial.ttf", 18 );

		PlayerComponent player = parent.get!PlayerComponent;

		SDL_Renderer * ren = parent.world.getRenderer();
		int a = player.getPlayer();
		bmp = TTF_RenderText_Solid(f, "Player-", c);
		tex = SDL_CreateTextureFromSurface(ren, bmp);
	}

	void render(SDL_Renderer * ren)
	{


		//writeln(ren);

		auto logic = parent.get!LogicComponent;

		InputComponent input = parent.get!InputComponent;




		/*
		if (input !is null) {
			immutable(char)* a;
			if (input.device)
				a = ("Input: " ~ to!string(player.getPlayer()) ~ " " ~ to!string(input.device.getID()) ~ " " ~ to!string(input.device.getPlayer())).toStringz();
			else
				a = ("Input: " ~ to!string(player.getPlayer()) ~ ": No device").toStringz();
			bmp = TTF_RenderText_Solid(f, a, c);
		} else {
			immutable(char)* a = ("Null: " ~ to!string(player.getPlayer())).toStringz();
			bmp = TTF_RenderText_Solid(f, a, c);
		}
		*/
		
		SDL_Rect src;
		SDL_Rect dst;

		src.x = 0;
		src.y = 0;
		src.w = 100;
		src.h = 100;


		dst.x = logic.x;
		dst.y = logic.y;
		dst.w = 600;
		dst.h = 100;



		
		//Draw the texture
		SDL_RenderCopy(ren, tex, null, &dst);
	}
}

