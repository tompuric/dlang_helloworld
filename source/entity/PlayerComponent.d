﻿module entity.PlayerComponent;
import entity.Component;
import entity.ObjectFactory;
import entity.InputComponent;

import std.conv;
import std.stdio;

class PlayerComponent : Component
{

	ObjectFactory.Player player;

	this()
	{
	}

	this(ObjectFactory.Player player)
	{
		this.player = player;
	}
	
	override void initialize()
	{
		
	}
	
	void update()
	{

	}

	int getPlayer()
	{
		return to!int(player);
	}
}

