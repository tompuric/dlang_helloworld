﻿module entity.InputComponent;
import core.InputSystem;
import io.InputController;
import entity.Component;
import entity.LogicComponent;
import entity.MovementComponent;

import std.stdio;
import io.controller.Device;

class InputComponent : Component
{

	InputController input;
	Device device;

	this()
	{
	}

	this(Device device)
	{
		attach(device);
	}

	this(InputController input)
	{
		this.input = input;
	}

	void attach(Device device)
	{
		this.device = device;
		device.register(this);
		this.input = device.controller;
	}

	void add()
	{

	}

	void unregister()
	{
		//parent.remove(this);
		writeln("We've unregistered this controller");
		input = null;
		device = null;
	}

	void update()
	{
//		if (!input) return;
//
//		auto logic = parent.get!MovementComponent;
//		//assert(logic !is null, "Missing MovementComponent");
//
//		if (logic)
//		{
//			if (input.left.pressed)
//			{
//				logic.moveLeft(input.left.power);
//			}
//			if (input.right.pressed)
//			{
//				logic.moveRight(input.right.power);
//			}
//			if (input.up.pressed)
//			{
//				logic.moveUp(input.up.power);
//			}
//			if (input.down.pressed)
//			{
//				logic.moveDown(input.down.power);
//			}
//		}
		 


	}

	bool needsDevice()
	{
		return input is null;
	}

}

