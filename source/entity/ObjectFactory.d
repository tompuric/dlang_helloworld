﻿module entity.ObjectFactory;
import entity.GameObject;
import entity.GraphicsComponent;
import entity.LogicComponent;
import entity.InputComponent;
import entity.MovementComponent;
import io.InputController;
import std.meta;
import std.stdio;
import io.controller.Device;
import entity.PlayerComponent;
import entity.InputPlayerComponent;
import entity.Component;
import world.world;

class ObjectFactory
{

	World world;
	alias PlayerComponents = AliasSeq!(GraphicsComponent, LogicComponent, MovementComponent, InputPlayerComponent, PlayerComponent);

	enum Player {
		PLAYER1 = 0,
		PLAYER2,
		PLAYER3,
		PLAYER4,
		UNASSIGNED
	}

	enum ObjectType
	{
		Player,
		Monster
	}

	this(World world)
	{
		this.world = world;
	}

	private GameObject create(GameObject object)
	{
		object.init(world);
		return object;
	}

	private GameObject create(T...)() {
		Component[] components;

		foreach(C; T) 
		{
			components ~= new C();
		}

		return create(new GameObject(components));
	}

	auto createPlayer(void delegate(GameObject) setup)
	{
		auto player = create!(GraphicsComponent, LogicComponent, MovementComponent, InputPlayerComponent, PlayerComponent);

		if (setup !is null) 
		{
			setup(player);
		}

		return player;
	}

	GameObject createPlayer(Player player, float x, float y)
	{
		return createPlayer(
			(GameObject object) => object.get!LogicComponent.setLocation(x, y)
			);
	}
	
	GameObject createPlayerInput(Device device)
	{
		writeln("Why is this being created?");
		return new GameObject([
				new InputComponent(device)
			]);
	}

	version(none) auto createObject(C, T...)(T t)
	{
		Component[] components;
		
		foreach(X; C) 
		{
			components ~= new X();
		}

		GameObject gameObject = new GameObject(components);

		foreach(X; C)
		{
			foreach(i, F; T)
			{
				static if(__traits(compiles, t[i](gameObject.get!X)))
				{
					t[i](gameObject.get!X);
				}
			}
		}

		return gameObject;
	}

	/*
	GameObject getPlayer(int playerIndex)
	{
		foreach(object; objects)
		{
			if (object.has!PlayerComponent)
			{
				PlayerComponent player = object.get!PlayerComponent;
				if (player.getPlayer() is playerIndex)
				{
					return object;
				}
			}
		}

		return null;
	}*/
}

