﻿module entity.TextComponent;

import derelict.sdl2.sdl;
import derelict.sdl2.ttf;
import std.stdio;
import entity.Component;

class TextComponent : Component
{

	SDL_Surface *bmp;
	SDL_Texture *tex;

	SDL_Color c = { 255, 255, 255 };
	TTF_Font *f;

	string text;

	this(string text)
	{
		// Constructor code
		this.text = text;
	}

	override void initialize()
	{
		f = TTF_OpenFont( "/Library/Fonts/Arial.ttf", 18 );
		bmp = TTF_RenderText_Solid(f, "Hello", c);
	}

	void render(SDL_Renderer * ren)
	{
	}
}

