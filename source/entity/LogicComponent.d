﻿module entity.LogicComponent;

import entity.Component;
import core.InputSystem;
import io.InputController;
import derelict.sdl2.sdl;
import std.stdio;
import std.conv;
import std.math;
import io.controller.Device;

class LogicComponent : Component
{
	int x;
	int y;

	this()
	{
	}

	this(float x, float y)
	{
		this.x = to!int(x);
		this.y = to!int(y);
	}

	override void initialize()
	{

	}

	void setLocation(float x, float y)
	{
		this.x = to!int(x);
		this.y = to!int(y);
	}

	void update()
	{

	}

}

