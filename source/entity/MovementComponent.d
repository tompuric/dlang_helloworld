﻿module entity.MovementComponent;

import entity.Component;
import core.InputSystem;
import io.InputController;
import derelict.sdl2.sdl;
import std.stdio;
import std.conv;
import std.math;
import io.controller.Device;
import entity.LogicComponent;

class MovementComponent : Component
{
	float vx;
	float vy;
	float speed;
	
	this()
	{
		vx = vy = 0.0f;
		speed = 10.0f;
		
	}

	override void initialize()
	{
	}
	
	void update()
	{	
		auto logic = parent.get!LogicComponent;
		logic.x += to!int(vx);
		logic.y += to!int(vy);

		vx = 0.0f;
		vy = 0.0f;
	}
	
	void moveLeft(float power)
	{
		vx = -speed * abs(power);
	}
	
	void moveRight(float power)
	{
		vx = speed * abs(power);
	}
	
	void moveUp(float power)
	{
		vy = -speed * abs(power);
	}
	
	void moveDown(float power)
	{
		vy = speed * abs(power);
	}
}

