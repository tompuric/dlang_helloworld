﻿module Engine;

import std.stdio;
import std.string;
import std.file;
import std.utf : byChar;

import derelict.sdl2.sdl;
import derelict.sdl2.image;
import derelict.sdl2.mixer;
import derelict.sdl2.ttf;
import derelict.sdl2.net;
import derelict.sdl2.types;

import world.world;

import core.io.InputGatherer;
import core.LogicSystem;
import core.GraphicsSystem;
import entity.GameObject;

import entity.GraphicsComponent;
import entity.LogicComponent;
import entity.ObjectFactory;

import sdl;
import entity.MovementComponent;
import entity.InputComponent;
import io.controller.Device;
import world.Space;

class Engine
{

	bool running;
	World world;
	sdl framework;

	InputGatherer input;

	Space[] spaces;

	this()
	{
		world = new World(this);
		input = new InputGatherer(this);


		init();
	}

	public void start()
	{
		run();
	}


	/*
	 * Initialise SDL variables
	 */
	void init()
	{
		framework = new sdl();
		framework.init();

		running = true;

		world.init();
	}

	/*
	 * Run game loop
	 */
	void run() {
		SDL_Renderer* ren = framework.getRenderer();

		while(running)
		{

			//First clear the renderer
			SDL_RenderClear(ren);

			update(spaces);
			render(spaces);

			// Present
			SDL_RenderPresent(ren);

			// Sleep
			SDL_Delay(10);
		}
	}

	void update(Space[] spaces)
	{
		input.update();

		foreach(space; spaces)
		{
			if (!space.active) return;
			world.update(space);
		}
	}

	void render(Space[] spaces)
	{
		foreach(space; spaces)
		{
			if (!space.active) return;
			world.render(space);
		}
	}

	public void getAudioManager()
	{
	}

	SDL_Renderer* getRenderer()
	{
		return framework.getRenderer();
	}

	public void getOtherRandomManagers()
	{
	}

	public void attachDevice(Device device)
	{
		foreach(space; spaces)
		{
			// Notify the spaces that a device has been plugged in
			if (!space.active) return;
			space.addDevice(device);
		}
	}

	Space getSpace(string name)
	{
		foreach (space; spaces)
		{
			if (space.getName() is name)
			{
				return space;
			}
		}
		return null;
	}

	// By Default: At least one space is allowed an input manager
	// An inout should own a space. Scenarios:
	//	1. If a player pauses the game, only that player can control/unpause. Pass the space that players Input/GameObject
	//	2. If a player selects a start lobby menu, should that player be first player? Assumingly so.
	//	3. During a game where there is some HUD, the HUD needs an Input if input is required
	//	(not necessarily needed for a multi-player game)
	//	4. 

	void createSpace(string name)
	{
		Space space = new Space(this, name);
		spaces ~= space;
	}

}

